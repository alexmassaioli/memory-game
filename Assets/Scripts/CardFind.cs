﻿using UnityEngine;
using System.Collections;

public class CardFind : MonoBehaviour {

	public int numberOfCardsFliped;
	public GameObject firstCardFlipped = null;
	public GameObject secondCardFlipped = null;
	public int pairsFound;
	
	public static void rotateCard(GameObject card)
	{
		card.transform.Rotate(0, 0, 180);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Mouse0))
		{
			RaycastHit objectHit;
			Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
			if(Physics.Raycast(cameraRay, out objectHit))
			{
				if(numberOfCardsFliped == 2)
				{
					rotateCard(firstCardFlipped);
					rotateCard(secondCardFlipped);
					firstCardFlipped = null;
					secondCardFlipped = null;
					numberOfCardsFliped = 0;
				}
				rotateCard(objectHit.collider.gameObject);
				if(firstCardFlipped == null)
				{
					firstCardFlipped = objectHit.collider.gameObject;
				}
				else
				{
					secondCardFlipped = objectHit.collider.gameObject;

				}
				numberOfCardsFliped++;
				if(secondCardFlipped != null && firstCardFlipped != null)
				{
					Card cardOne = firstCardFlipped.GetComponent<Card>();
					Card cardTwo = secondCardFlipped.GetComponent<Card>();
					if(cardOne.getNumber() == cardTwo.getNumber())
					{
						Destroy(firstCardFlipped);
						Destroy(secondCardFlipped);
						firstCardFlipped = null;
						secondCardFlipped = null;
						numberOfCardsFliped = 0;
						pairsFound++;
						GameObject.Find("Score").GetComponent<PointsManager>().increasePoints();
					}
				}
			}
		}
	}
}




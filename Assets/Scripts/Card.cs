﻿using UnityEngine;
using System.Collections;
using System;

public class Card : MonoBehaviour
{
	private int cardNumber;

	public int getNumber()
	{
		return cardNumber;
	}

	public void setNumber(int newNumber)
	{
		cardNumber = newNumber;
	}
}
﻿using UnityEngine;
using System.Collections;

public class PointsManager : MonoBehaviour {

	private int points;

	public void increasePoints()
	{
		points++;
		GameObject.Find("Score").GetComponent<TextMesh>().text = points.ToString();

	}

	// Use this for initialization
	void Start () {

		points = 0;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardSpawn : MonoBehaviour {

	public GameObject cardToSpawn;
	public int rangeCount;
	public int halfWidth;

	private int pairRequired;

	private static List<int> shuffle(List<int> sortedList)
	{
		List<int> unsortedList = new List<int>();
		while(0 < sortedList.Count)
		{
			int selectedElement = Random.Range(0,sortedList.Count);
			int numberToAdd =  sortedList[selectedElement];
			unsortedList.Add(numberToAdd);
			sortedList.RemoveAt(selectedElement);
		}
		return unsortedList;
	}

	private static void cardSpawn(GameObject card, List<int> numbers, int gridWidth)
	{
		for(int xposition = 0; xposition < gridWidth; xposition++)
		{
			for(int zposition = 0; zposition < gridWidth; zposition++)
			{

				GameObject newCard = (GameObject) Instantiate(card,new Vector3(xposition,0,zposition),Quaternion.identity);

				int firstNumber = numbers[0];
				numbers.RemoveAt(0);

				Card cardModel = newCard.GetComponent<Card>();
				cardModel.setNumber(firstNumber);

				TextMesh cardNumberText = newCard.transform.FindChild("CardText").GetComponent<TextMesh>();
				cardNumberText.text = firstNumber.ToString();
			}
		}
	}


	// Use this for initialization
	void Start () {

		Camera.main.transform.position = new Vector3((float)halfWidth - 0.5f, 5, (float)halfWidth - 0.5f);
		Camera.main.orthographicSize = halfWidth + 1;

		pairRequired = ((2 * halfWidth)*(2 * halfWidth)) / 2;
		Debug.Log(pairRequired);

		List<int> cardNumbers = new List<int>();
		for(int x = 0; x < pairRequired; x++)
		{
			cardNumbers.Add(x);
			cardNumbers.Add(x);
		}

		cardNumbers = shuffle(cardNumbers);
	
		cardSpawn(cardToSpawn, cardNumbers, halfWidth * 2);

	}
	
	// Update is called once per frame
	void Update () {
	


	}
}
